import React from 'react';

function Footer(props) {
    return(
    <div className="footer">
        <div className="container">
            <div className="row justify-content-center">
              {/*Reference to other pages.Other pages currently not available*/ }
                <div className="col-4 offset-1 col-sm-2">
                    <h5>Links</h5>
                    <ul className="list-unstyled">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Menu</a></li>
                        <li><a href="contactus.html">Contact</a></li>
                    </ul>
                </div>
              {  /*Contact Details */  }
                <div className="col-7 col-sm-5">
                    <h5>Our Address</h5>
                    <address>
        		              574, ABC Zone <br />
                          Knowledge Park<br />
                          DELHI<br />
                          <i className="fa fa-phone fa-lg"></i>: +874 1237 7898<br />
        		              <i className="fa fa-fax fa-lg"></i>: +584 9652 4178<br />
        		              <i className="fa fa-envelope fa-lg"></i>: <a href="mailto:amritash3037@gmail.com">
                                 connect with mail</a>
                    </address>
                </div>
              {  /*Links to accounts of the Company on different platforms.*/ }
                <div className="col-12 col-sm-4 align-self-center">
                    <div className="text-center">
                        <a className="btn btn-social-icon btn-google" href="http://google.com/+"><i className="fa fa-google-plus"></i></a>
                        <a className="btn btn-social-icon btn-facebook" href="http://www.facebook.com/profile.php?id="><i className="fa fa-facebook"></i></a>
                        <a className="btn btn-social-icon btn-linkedin" href="http://www.linkedin.com/in/"><i className="fa fa-linkedin"></i></a>
                        <a className="btn btn-social-icon btn-twitter" href="http://twitter.com/"><i className="fa fa-twitter"></i></a>
                        <a className="btn btn-social-icon btn-google" href="http://youtube.com/"><i className="fa fa-youtube"></i></a>
                        <a className="btn btn-social-icon" href="mailto:"><i className="fa fa-envelope-o"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    )
}

export default Footer;
