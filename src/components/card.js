import React from 'react';
import '../App.css';

export default function Card(props){
  return (
    <div className =" card container align-self-center" style={{width:'60rem'}} >

      { /* Rendering the Avatar of member */ }
       <div className="container">
            <div className ="col-12 col-sm-4">
            <img src= {props.image}  />
            </div>

      {  /* Rendering the Card Details of each member */ }
            <div className="col-12 col-sm-6">
               <h2> {props.name}</h2>
               <h6>{props.description}</h6>
               <h6>Address - {props.streetaddress}, {props.secondaryaddress} - {props.zip}, {props.country}</h6>
               <h6>Company -{props.company}</h6>
            </div>
        </div>

    </div>


  )
}
