import React, { Component } from 'react';
import { Navbar, NavbarBrand, Jumbotron } from 'reactstrap';
import pic from '../logodesign.jpg';

class Header extends Component {
  render() {
    return(
    <div>
  {/* Navigation Bar */}
        <Navbar >
          <div className="container">
              <NavbarBrand href="/">
                <h3>Career Recruiting Platform</h3>
              </NavbarBrand>
              <div>
                 <img src={pic} alt="logo" width="65px" height="50px" className="responsive" />
              </div>
              <ol className="col-12 breadcrumb">
                <li className="breadcrumb-item"><a href="/">Home</a></li>
                <li className="breadcrumb-item"><a href="/">About Us</a></li>
                <li className="breadcrumb-item"><a href="/">Menu</a></li>
                <li className="breadcrumb-item"><a href="/">Content</a></li>
            </ol>

          </div>
        </Navbar>
   {/*Jumbotron */  }
        <div className="Jumbotron">
             <div className="container">
                 <div className="row row-header">
                     <div className="col-12 col-sm-6">
                         <h1 class="display-4">Front FOOT</h1>
                         <h4 class="lead"> Unify the teams, systems, and data that drive your staffing and recruiting business.</h4>
                         <br/>
                         <h5 className="lead">With Front FOOT, you can search millions of jobs online to find the next step in your career. With tools for job search, resumes, company reviews and more connect to the World of Jobs. </h5>
                     </div>
                     <div className="col-12 col-sm-6">
                       <img src={pic} alt="logo" width="400px" height="200px" />
                     </div>
                 </div>
             </div>
         </div>

    </div>
    );
  }
}

export default Header;
