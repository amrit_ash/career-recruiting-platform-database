import React from 'react';
import Header from "./components/ HeaderComponent.js";
import Footer from './components/FooterComponent';
import {Navbar,NavbarBrand,Jumbotron} from 'reactstrap';
import Card from './components/card';
import faker from 'faker';
import './App.css';

 function App(){
   const members=['one','two','three','four'];

   return(
     <div >
      { /* Header Details */}
       <Header/>
      { /* Cards */ }
       <div className="gs">
            <ul>

              {/*Looping over each member of members*/}
                {members.map((member)=>{
                 return <li key={member}>
                   <div className="container ">

                        <Card name={faker.name.firstName()} description={faker.lorem.sentence()} streetaddress={faker.address.streetAddress()} secondaryaddress={faker.address.secondaryAddress()} zip={faker.address.zipCode()} country={faker.address.country()} company={faker.company.companyName()}  image= {faker.image.avatar()} />

                   </div>
                  </li>
                })}
            </ul>
        </div>
        {/*Footer Details */ }
       <Footer />

   </div>
 );}

export default App;
